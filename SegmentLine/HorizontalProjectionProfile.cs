﻿using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using AForge.Imaging;
using Accord.Math;
using Accord.Imaging.Converters;
using OCR.Nepali.Segmentation.Core;

namespace SegmentLine
{
    public static class HorizontalProjectionProfile
    {
        internal static byte[,] ImageByteArray;
        public static Dictionary<int, byte[,]> lineBMs = new Dictionary<int, byte[,]>();
        public static List<Bitmap> LineBitmaps { get; set; }
        public static Bitmap wholeImage;        

        internal class HPPIndexer
        {
            public IEnumerable<int> this[Bitmap image]
            {
                get
                {
                    new ImageToMatrix().Convert(image, out ImageByteArray);
                    for (int i = 0; i < ImageByteArray.GetLength(0); i++)
                    {
                        var c = ImageByteArray.GetRow<byte>(i);
                        var blackPixelCount = c.Where(x => x == (int)PixelValue.Black).Count();
                        yield return blackPixelCount;
                    }
                }
            }
        }

        public static VerticalIntensityStatistics VerticalIntensities(Bitmap sourceImage)
        {
            // collect vertical pixel distribution
            return new VerticalIntensityStatistics(sourceImage);
        }

        public static HorizontalIntensityStatistics HorizontalIntensities(Bitmap sourceImage)
        {
            // collect horizontal pixel distribution
            return new HorizontalIntensityStatistics(sourceImage);
        }

        public static Bitmap AttachLineHistos(this Bitmap image)
        {
            //Attaching histogram with image
            var histoGrammedBitmap = new Bitmap(image);
            var bins = new HPPIndexer()[image].ToList();

            //Non-smoothed
            using (Graphics gr = Graphics.FromImage(histoGrammedBitmap))
            {
                for (int y = 0; y < bins.Count; y++)
                {
                    float value = 300 * (float)bins[y] / bins.Count;
                    gr.DrawLine(Pens.Blue, new Point(0, y), new PointF(value, y));
                }
            }

            //var pdfEstimate = Gaussian.Estimate(bins.ToArray().Select(x => (double)x).ToArray()).ToArray();

            //Smoothing
            //using (Graphics gr = Graphics.FromImage(histoGrammedBitmap))
            //{
            //    for (int y = 0; y < pdfEstimate.Length; y++)
            //    {                    
            //        // Averaging with window-size of 3 (Averaging filter)
            //        var mean = ((y == 0 ? 0 : pdfEstimate[y - 1]) + pdfEstimate[y] + (y == pdfEstimate.Length - 1 ? 0 : pdfEstimate[y + 1])) / 3;
            //        float value = 30000 * (float)mean;
            //        gr.DrawLine(Pens.Blue, new Point(0, y), new PointF(value, y));
            //    }
            //}

            return histoGrammedBitmap;
        }

        public static List<Bitmap> SegmentLines(this Bitmap pageImage, out Bitmap lineRectangledPage)
        {            
            List<Bitmap> lineImages = new List<Bitmap>();
            List<int> linePixels = new List<int>();            
            wholeImage = pageImage;

            // Getting horizontal pixel distribution and filtering to relevant text rows (Ignoring white spaces)
            List<int> bins = new HPPIndexer()[pageImage].ToList();
            for (int i = 0; i < bins.Count; i++)
            {
                if (bins[i] != 0)
                    linePixels.Add(i);                
            }
            
            //Clustering text lines on white spaces (i.e. discontinuity of row pixel data)
            List<List<int>> lineClusters = linePixels.ExtractBins();            

            //Minor cleaning, avoiding lines with height less than half of average line height
            var avLineHeight = lineClusters.Average(clus => clus.Count);
            lineClusters.RemoveAll(c => c.Count < avLineHeight / 2);

            //Wrap rectangles around line segments
            lineRectangledPage = new Bitmap(pageImage);
            using (Graphics g = Graphics.FromImage(lineRectangledPage))
            {
                int width = lineRectangledPage.Width;
                foreach (var bin in lineClusters)
                {
                    g.DrawRectangle(new Pen(Brushes.Red, 2),
                        new Rectangle(new Point(0, bin[0]), new Size(width, bin[bin.Count - 1] - bin[0] + 1)));
                }
            }

            //Array(clusters elements) to image conversion
            lineClusters.ForEach(cl =>
            {
                Bitmap line;
                var lineByteArray = ImageByteArray.SubMatrixCustom<byte>(cl.ToArray(), null);
                lineBMs.Add(cl[0], lineByteArray);
                new MatrixToImage().Convert(lineByteArray, out line);
                lineImages.Add(line);
            });

            LineBitmaps = lineImages;
            return lineImages;
        }
    }
}