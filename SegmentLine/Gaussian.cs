﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Statistics;

namespace SegmentLine
{
    public class Gaussian
    {
        public static IEnumerable<double> Estimate(double[] data)
        {
            var mean = data.Average();
            var sd = Tools.StandardDeviation(data, true);
            var pdf = data.Select(x => (1/(sd * Math.Sqrt(2 * Math.PI))) * Math.Exp(-(x - mean)/(2*sd*sd)));
            return pdf;
        }
    }
}
