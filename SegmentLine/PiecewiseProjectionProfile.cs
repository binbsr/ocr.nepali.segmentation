﻿using Accord.Imaging.Converters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ThresholdingAndBinarization;
using OCR.Nepali.Segmentation.Core;
using Accord.Math;

namespace SegmentLine
{
    public class PiecewiseProjectionProfile
    {
        public static double AvLineHeight { get; set; }

        public static Bitmap Build(Bitmap image, Bitmap orgImage, out Bitmap profileImage)
        {
            var binaryImage = BinarizationAdaptive.BradelyLocalThreshold(GrayScale.GrayBT709(image));
            var tempBinaryImage = new Bitmap(orgImage);
            byte[,] binaryImageBuffer;
            new ImageToMatrix().Convert(binaryImage, out binaryImageBuffer);
            int imgHeight = binaryImage.Height;
            IList<int> profileArray = new List<int>(imgHeight);
            IList<int> profileArraySmoothed = new List<int>(imgHeight);
            IList<int> initialDesArray = new List<int>(imgHeight);
            IList<int> initialDesArraySmoothed = new List<int>(imgHeight);
            IList<int> startValleys = new List<int>();
            IList<int> valleys = new List<int>();
            IList<IList<int>> allDocValleys = new List<IList<int>>();
            IList<IList<KeyValuePair<int, int?>>> allDocClosestPairs = new List<IList<KeyValuePair<int, int?>>>();

            // Divide document across its width into chunks of 5% each
            var docWidth = binaryImage.Width;
            var chunkWidth = docWidth / 15;  //5%
            var surplusWidth = (docWidth % 15) / 15;  //Rest
            chunkWidth += surplusWidth; //Distributing rest to all
            var initialDesWidth = docWidth / 4; //25%

            // Finding valleys of First 25% of doc to obtain starting positions of the lines
            initialDesArray = CreateProfile(binaryImageBuffer, 0, initialDesWidth);
            initialDesArraySmoothed = SmoothingMovingAverage(imgHeight, initialDesArray);
            startValleys = GetValleys(initialDesArraySmoothed);
            PiecewiseProjectionProfile.AvLineHeight = GetAvTextLineHeight(startValleys);

            //Complete set of candidate lines, starting from second chunk
            allDocValleys.Add(startValleys);// Starter
            for (int x = chunkWidth; x < docWidth; x += chunkWidth)
            {
                profileArray = CreateProfile(binaryImageBuffer, x, chunkWidth);
                profileArraySmoothed = SmoothingMovingAverage(imgHeight, profileArray);
                valleys = GetValleys(profileArraySmoothed);
                allDocValleys.Add(valleys);
            }

            //Get nearest valleys for whole document
            for (int x = 0; x < allDocValleys.Count - 1; x++)
            {
                var closestPairs = GetNearestValleys(allDocValleys[x], allDocValleys[x + 1]);
                allDocClosestPairs.Add(closestPairs);
            }

            //Drawing: Initial separators and Projection profiles of each chunk: Smoothing with moving average of fixed window-size   
            var lineBmpImage = new Bitmap(tempBinaryImage);
            Graphics gr = Graphics.FromImage(tempBinaryImage);
            for (int x = 0; x < docWidth; x += chunkWidth)
            {
                profileArray = CreateProfile(binaryImageBuffer, x, chunkWidth);
                profileArraySmoothed = SmoothingMovingAverage(imgHeight, profileArray);

                // Drawing each chunk, uncomment to see splits
                gr.DrawLine(Pens.DarkBlue, new Point(x, 0), new Point(x, imgHeight));

                //Profiles
                for (int y = 0; y < imgHeight; y++)
                {
                    gr.DrawLine(Pens.Chocolate, new Point(x, y), new Point(x + profileArraySmoothed[y], y));
                }
            }

            profileImage = new Bitmap(tempBinaryImage);
            //gr.Clear(Color.Black);
            gr = Graphics.FromImage(lineBmpImage);

            //1: Conecting valleys of i+1 to i horizontally
            var i = 0;
            foreach (var vals in allDocValleys)
            {
                foreach (var val in vals)
                {
                    gr.DrawLine(new Pen(Brushes.DarkBlue, 2), new Point(i, val - 5), new Point(i + chunkWidth, val - 5));
                }
                i += chunkWidth;
            }

            //2: Connecting closest line separators in 1, vertically
            i = chunkWidth;
            foreach (var closestPairs in allDocClosestPairs)
            {
                foreach (var closestPair in closestPairs)
                {
                    if (closestPair.Value != null)
                        gr.DrawLine(new Pen(Brushes.DarkBlue, 2), new Point(i, closestPair.Key - 5), new Point(i, (int)closestPair.Value - 5));
                    else
                        gr.DrawLine(new Pen(Brushes.DarkBlue, 2), new Point(i, closestPair.Key - 5), new Point(i + chunkWidth, closestPair.Key - 5));
                }
                i += chunkWidth;
            }
            gr.Dispose();

            return lineBmpImage;
        }

        private static double GetAvTextLineHeight(IList<int> arr)
        {
            var sum = 0;
            for (int i = 0; i < arr.Count - 1; i++)
                sum += arr[i + 1] - arr[i];
            return sum / (double)(arr.Count - 1);
        }

        private static IList<KeyValuePair<int, int?>> GetNearestValleys(IList<int> left, IList<int> right)
        {
            var connecters = new List<KeyValuePair<int, int?>>();
            var nearThresold = PiecewiseProjectionProfile.AvLineHeight;
            //Connect a valley from "right" to the closest valley in "left"
            foreach (var p in left)
            {
                int? minDis = int.MaxValue, minPoint = null;
                KeyValuePair<int, int?> connector;
                foreach (var q in right)
                {
                    var diff = Math.Abs(p - q);
                    if (diff < minDis && diff < nearThresold)
                    {
                        minPoint = q;
                        minDis = diff;
                    }
                }

                connector = new KeyValuePair<int, int?>(p, minPoint);

                //If two or more valleys from "right" are connected to the same valley in "left", retain the closest pair and reject the rest
                if (minPoint != null)
                {
                    var valleyUsed = connecters.Where(x => x.Value == minPoint);
                    if (valleyUsed.Any())
                    {
                        var pair = valleyUsed.First();
                        if (Math.Abs(pair.Key - (int)minPoint) > minDis)
                        {
                            connecters.Remove(pair);
                            connecters.Add(connector); //Add closest
                        }
                    }
                    else
                        connecters.Add(connector);
                }
            }

            //There can still remain a few valleys in "left" not connected to any in "right". 
            //In such cases, line is continued to be drawn straight from the valley in "left".
            //And these are returned as null values in keyValuePair's list

            return connecters;
        }

        /// <summary>
        /// Returns index array where valley lies
        /// </summary>
        /// <param name="arraySmoothed"></param>
        /// <returns></returns>
        //private static List<int> GetValleys(List<int> arraySmoothed)
        //{
        //    List<int> valleys = new List<int>();
        //    List<int> tempValley = new List<int>();

        //    int slope = 0, globalMin = arraySmoothed.Min();


        //    for (int i = 1; i < arraySmoothed.Count - 1; i++)
        //    {
        //        // A[i] is minima if A[i-1] >= A[i] <= A[i+1]
        //        bool isValley = (arraySmoothed[i - 1] >= arraySmoothed[i]) && (arraySmoothed[i] <= arraySmoothed[i + 1]);

        //        // If several equal minima values for same valley, average the indexes keeping in temp list
        //        if (isValley)
        //        {
        //            tempValley.Add(i);
        //        }
        //        else
        //        {
        //            if (tempValley.Any())
        //            {
        //                if (arraySmoothed[i] <= arraySmoothed[i + 1] && slope == -1)
        //                {
        //                    valleys.Add((int)tempValley.Average());
        //                }

        //                tempValley.Clear();
        //            }
        //        }
        //        if (arraySmoothed[i - 1] > arraySmoothed[i])
        //        {
        //            slope = -1;
        //        }
        //        else if (arraySmoothed[i - 1] < arraySmoothed[i])
        //        {
        //            slope = 1;
        //        }
        //    }

        //    return valleys;
        //}

        private static IList<int> GetValleys(IList<int> arraySmoothed)
        {
            var valleys = new List<int>();
            var tempValley = new List<int>();

            var threshold = 0.75 * arraySmoothed.Average(); //Dynamic Threshold
            for (int i = 0; i < arraySmoothed.Count; i++)
            {
                if (arraySmoothed[i] <= threshold)
                {
                    tempValley.Add(i);
                }
                else
                {
                    if (tempValley.Any())
                    {
                        if (!tempValley.Contains(0))    //Removing top separator  && (tempValley.Count > 10 && tempValley.Count < 50)
                        {                           
                            valleys.Add((int)tempValley.Average());
                        }
                        tempValley.Clear();
                    }
                }
            }
            return valleys;
        }

        //private static IList<int> GetSeparatorValleys(IList<KeyValuePair<int, int>> valleys)
        //{
        //    var res = new List<int>();

        //    // Filter 1: Exclude valley if black pixel count is greater than the global avergae
        //    var valleyPixelCntAv = valleys.Average(x => x.Value);
        //    res = valleys.Where(x => x.Value < valleyPixelCntAv).Select(y => y.Key).ToList();

        //    // Filter 2: Assuming text line height to be similar for all lines in the text, exclude remaining separators if close enough to right one
        //    var diffList = new List<int>();
        //    var resFilter2 = new List<int>();
        //    for (int i = 0; i < res.Count - 1; i++)
        //        diffList.Add(res[i + 1] - res[i]);
        //    var diffExcludeThreshold = diffList.Average() / 2;
        //    for (int i = 0; i < res.Count - 1; i++)
        //        if (res[i + 1] - res[i] > diffExcludeThreshold)
        //        {
        //            resFilter2.Add(res[i]);
        //            if (i == res.Count - 2)
        //                resFilter2.Add(res[i + 1]);
        //        }
        //        else
        //        {
        //            resFilter2.Add(valleys.First(x => x.Key == res[i]).Value < valleys.First(x => x.Key == res[i + 1]).Value ? res[i] : res[i + 1]);
        //            i++;
        //        }
        //    return resFilter2;
        //}
        ////KeyValuePair<index, pixelCount>
        //private static IList<KeyValuePair<int, int>> GetValleys(IList<int> a)
        //{
        //    var res = new List<KeyValuePair<int, int>>();
        //    if (a.Count < 2)
        //    {
        //        return res;
        //    }
        //    int lastEq = 0, eqMiddle;
        //    CurveState s = CurveState.NotGoingDown;
        //    for (var i = 1; i != a.Count; i++)
        //    {
        //        switch (Math.Sign(a[i] - a[i - 1]))
        //        {
        //            case -1:
        //                s = CurveState.GoingDown;
        //                break;
        //            case 0:
        //                if (s == CurveState.GoingDown)
        //                {
        //                    lastEq = i;
        //                }
        //                s = (s == CurveState.NotGoingDown) ? CurveState.NotGoingDown : CurveState.EqGoingDown;
        //                break;
        //            case 1:
        //                if (s == CurveState.GoingDown)
        //                {
        //                    res.Add(new KeyValuePair<int, int>(i - 1, a[i - 1]));
        //                }
        //                else if (s == CurveState.EqGoingDown)
        //                {
        //                    eqMiddle = (lastEq + i - 1) / 2;
        //                    res.Add(new KeyValuePair<int, int>(eqMiddle, a[eqMiddle]));
        //                }
        //                s = CurveState.NotGoingDown;
        //                break;
        //        }
        //    }
        //    return res;
        //}

        private static IList<int> CreateProfile(byte[,] binaryImageBuffer, int toSkip, int toKeep)
        {
            int imgHeight = binaryImageBuffer.GetLength(0);
            var profileArray = new List<int>(imgHeight);
            for (int y = 0; y < imgHeight; y++)
            {
                var row = binaryImageBuffer.GetRow<byte>(y).Skip(toSkip).Take(toKeep);
                var blpxCnts = row.Where(p => p == (byte)PixelValue.Black).ToList().Count;
                profileArray.Add(blpxCnts);
            }

            return profileArray;
        }

        private static IList<int> SmoothingMovingAverage(int imgHeight, IList<int> profileArray)
        {
            var smoothProfileArray = new List<int>(imgHeight);
            var index = 0;
            // Smoothing
            for (int i = 0; i < imgHeight; i++)
            {
                index = i;
                if (i < 3)
                    index = 3;
                else if (i > imgHeight - 4)
                    index = imgHeight - 4;

                //var av = (profileArray[i < 2 ? 0 : i - 2] + profileArray[i < 1 ? 0 : i - 1] + profileArray[i]
                //    + profileArray[i > imgHeight - 2 ? imgHeight - 1 : i + 1] + profileArray[i > imgHeight - 3 ? imgHeight - 1 : i + 2]) / 5;
                var av = (profileArray[index - 3] + profileArray[index - 2] + profileArray[index - 1] + profileArray[index]
                    + profileArray[index + 1] + profileArray[index + 2] + profileArray[index + 3]) / 7;
                smoothProfileArray.Add((int)av);
            }

            return smoothProfileArray;
        }
    }
}