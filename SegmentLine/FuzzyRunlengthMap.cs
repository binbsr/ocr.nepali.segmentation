﻿using System.Collections.Generic;
using System.Drawing;
using Accord.Imaging.Converters;
using OCR.Nepali.Segmentation.Core;
using ThresholdingAndBinarization;
using System.IO;

namespace SegmentLine
{
    public class FuzzyRunlengthMap
    {
        public static Bitmap Build(Bitmap img,  out Bitmap orgImage, int maxBlkCount = (int)FuzzyRunLength.MaxBlockCount,
            int binThreshold = (int)FuzzyRunLength.FuzzyImageBinarizationThreshold)
        {
            var inpImgBinary = BinarizationAdaptive.BradelyLocalThreshold(GrayScale.GrayBT709(img));
            orgImage = inpImgBinary;
            int[,] fuzzyImage = new int[inpImgBinary.Height, inpImgBinary.Width];
            byte[,] fuzzyImageThresholded = new byte[inpImgBinary.Height, inpImgBinary.Width];
            byte[,] inpImgArray;
            Bitmap outImg = new Bitmap(inpImgBinary);            

            new ImageToMatrix().Convert(inpImgBinary, out inpImgArray);
            Queue<int> blkQueue = new Queue<int>();
            int rows = inpImgArray.GetLength(0), cols = inpImgArray.GetLength(1);

            // For each row of an image buffer
            for (int i = 0; i < rows; i++)
            {
                //Scan each row of the input image from left-to-right
                for (int j = 1; j < cols; j++)
                {
                    if (inpImgArray[i, j] == (int)PixelValue.Black)
                        blkQueue.Enqueue(j);
                    fuzzyImage[i, j] = (blkQueue.Count > maxBlkCount)
                        ? j - blkQueue.Dequeue()
                        : fuzzyImage[i, j - 1] + 1;
                }
                blkQueue.Clear();

                //Scan each row of the input image from right-to-left
                for (int j = cols - 2; j >= 0; j--)
                {
                    if (inpImgArray[i, j] == (int)PixelValue.Black)
                        blkQueue.Enqueue(j);
                    fuzzyImage[i, j] = (blkQueue.Count > maxBlkCount)
                        ? blkQueue.Dequeue() - j
                        : fuzzyImage[i, j + 1] + 1;
                }
                blkQueue.Clear();
            }

            //Thresholded binarization
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {                    
                    fuzzyImageThresholded[i, j] = (fuzzyImage[i, j] < binThreshold) ? byte.MinValue : byte.MaxValue;
                }                
            }            

            //new MatrixToImage().Convert(fuzzyImageThresholded, out outImg);
            
            for (int i = 0; i < outImg.Height; i++)
            {
                for (int j = 0; j < outImg.Width; j++)
                {
                    if (fuzzyImageThresholded[i, j] == (int)PixelValue.Black)
                        outImg.SetPixel(j, i, Color.Black);
                }
            }

            return outImg;
        }

        //private static List<int> GetTextLines(byte[,] fuzzImageArray)
        //{
        //    var rowsAvHeight = new List<int>();
        //    var rows = fuzzImageArray.GetLength(0);
        //    var cols = fuzzImageArray.GetLength(1);


        //}
    }
}