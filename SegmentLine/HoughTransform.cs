﻿using OCR.Nepali.Segmentation.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThresholdingAndBinarization;

namespace SegmentLine
{
    public class HoughTransform
    {
        public static Bitmap Build(Bitmap image, out Bitmap lineImage)
        {
            Bitmap inpBinarized = ThresholdingAndBinarization.BinarizationAdaptive.BradelyLocalThreshold(GrayScale.GrayBT709(image));
            int width = image.Width, height = image.Height;
            Bitmap outImage = new Bitmap(width, height);
            Bitmap outLineImage = new Bitmap(image);
            double rBound = Math.Sqrt(width * width + height * height);
            double thetaBound = 2 * Math.PI;

            double delR = rBound / (double)width;
            double delTheta = thetaBound / (double)height;

            int[,] accumulator = new int[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (inpBinarized.GetPixel(x, y).B == (byte)PixelValue.Black)
                    {
                        for (int theta = 1; theta < height; theta++)
                        {
                            double r = x * Math.Cos(delTheta * (double)theta) + y * Math.Sin(delTheta * (double)theta);
                            int k = (int)((r / rBound) * width);
                            if (k >= 0 && k < width)
                                accumulator[k, theta]++;
                        }
                    }
                }
            }

            // Find max of accumulator
            int max = 0;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (accumulator[i, j] > max) max = accumulator[i, j];
                }
            }

            Graphics g = Graphics.FromImage(outImage);
            g.Clear(Color.Black);
            g.Dispose();

            // Grayscale bitmap
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int b = 0;                    
                    if (max != 0) b = (int)(((double)accumulator[x, y] / (double)max) * 255.0);
                    outImage.SetPixel(x, y, Color.FromArgb(b, b, b));
                }
            }

            //List<int[]> lines = new List<int[]>();
            //for (int i = 0; i < width; i++)
            //{
            //    for (int j = 0; j < height; j++)
            //    {
            //        if (accumulator[i, j] > (int)HoughTransForm.MaximaThreshold)
            //            lines.Add(new [] {i, j});
            //    }
            //}
                        
            //foreach (var line in lines)
            //{
            //    double x, y;
            //    for (int theta = 0; theta < height; theta++)
            //    {
            //        x = line[0] * Math.Cos(theta * line[1]);
            //        y = line[0] * Math.Sin(theta * line[1]);
            //        outLineImage.SetPixel((int)x, (int)y, Color.Blue);
            //    }                
            //}

            lineImage = outLineImage;
            return outImage;
        }
    }
}