﻿using System;

namespace OCR.Nepali.Segmentation.Core
{
    public static class Matrix
    {
        public static T[,] SubMatrixCustom<T>(this T[,] matrix, int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices == null && columnIndices == null)
                throw new InvalidOperationException("Stupid! you can't get submatrix without providing row and/or column indices, supply any/both you fool.");


            int subMatrixRowSize, iStart, iStop;
            int subMatrixColumnSize, jStart, jStop;

            if(rowIndices != null)
            {
                subMatrixRowSize = rowIndices.Length;
                iStart = rowIndices[0];
                iStop = rowIndices[subMatrixRowSize - 1];
            }
            else
            {
                subMatrixRowSize = matrix.GetLength(0);
                iStart = 0;
                iStop = subMatrixRowSize - 1;
            }

            if(columnIndices!= null)
            {
                subMatrixColumnSize = columnIndices.Length;
                jStart = columnIndices[0];
                jStop = columnIndices[subMatrixColumnSize - 1];
            }
            else
            {
                subMatrixColumnSize = matrix.GetLength(1);
                jStart = 0;
                jStop = subMatrixColumnSize - 1;
            }
            
            T[,] subMatrix = new T[subMatrixRowSize, subMatrixColumnSize]; 
          
            for (int i = iStart, subi = 0; i <= iStop; i++, subi++)
                for (int j = jStart, subj = 0; j <= jStop; j++, subj++)
                    subMatrix[subi, subj] = matrix[i, j];

            return subMatrix;
        }
    }
}
