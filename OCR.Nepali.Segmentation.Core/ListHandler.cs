﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCR.Nepali.Segmentation.Core
{
    public static class ListHandler
    {
        public static List<List<int>> ExtractBins(this List<int> list)
        {              
            List<int> cluster = new List<int>();
            List<List<int>> clusters = new List<List<int>>();

            for (int i = 0; i < list.Count - 1; i++)
            {
                int currValue = list[i];
                cluster.Add(currValue);
                var termination = i == list.Count - 2;
                var split = list[i + 1] != currValue + 1;

                if (split || termination)
                {
                    if (!split && termination)
                        cluster.Add(list[i + 1]);
                    clusters.Add(cluster);
                    if (!termination)
                        cluster = new List<int>();
                }
            }
            return clusters;
        }
    }
}