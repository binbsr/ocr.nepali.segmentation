﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCR.Nepali.Segmentation.Core
{
    public enum PixelValue 
    {
        Black = 0,
        White = 1
    }

    public enum FuzzyRunLength 
    {
        MaxBlockCount = 25,
        AverageStrokeWidth = 6,
        FuzzyImageBinarizationThreshold = 200
    }

    public enum HoughTransForm
    {
        MaximaThreshold = 380
    }

    public enum PPP
    {
        Mov_Av_Window_Size = 7,
        InitialLinesValleyDiff = 10,
        CandidateLinesValleyDiff = 5,
        NormalizationConstant = 60
    }

    public enum CurveState
    {
        GoingDown = 0, 
        EqGoingDown = 1, 
        NotGoingDown = 2
    }
}
