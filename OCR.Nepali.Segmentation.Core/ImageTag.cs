﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCR.Nepali.Segmentation.Core
{
    public class ImageTag
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
