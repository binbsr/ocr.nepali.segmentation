﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace OCR.Nepali.Segmentation.Core
{
    public class ImageHandler
    {
        public static byte[] ImageToArray(Image img)
        {
            ImageConverter ic = new ImageConverter();
            return (byte[])ic.ConvertTo(img, typeof(byte[]));
            
            //using(MemoryStream ms = new MemoryStream())
            //{
            //    img.Save(ms, ImageFormat.);
            //    return ms.ToArray();
            //}
        }

        public static Image ArrayToImage(byte[] arr)
        {
            ImageConverter ic = new ImageConverter();
            return (Bitmap)ic.ConvertFrom(arr);   


            //using (MemoryStream ms = new MemoryStream(arr))
            //{
            //    imgSize = (int)ms.Length / 1024;
            //    return Image.FromStream(ms); ;
            //}
        }
    }
}
