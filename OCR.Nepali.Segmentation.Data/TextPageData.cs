﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCR.Nepali.Segmentation.Data
{
    public partial class TextPageData
    {        
        public int Id { get; set; }        
        public byte[] ImageData { get; set; }        
        public virtual TextPage TextPage { get; set; }
    }
}
