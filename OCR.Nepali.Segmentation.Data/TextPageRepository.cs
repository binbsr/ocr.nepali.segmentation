﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OCR.Nepali.Segmentation.Data
{
    public interface ITextPageRepository : IDisposable
    {
        IList<TextPage> All { get; }
        IQueryable<TextPage> AllIncluding(params Expression<Func<TextPage, object>>[] includeProperties);
        TextPage Find(int id);
        void InsertOrUpdate(TextPage page);
        void Delete(int id);
        void Save();
    }    

    public class TextPageRepository: ITextPageRepository
    {
        TextPageEntities context;
        public TextPageRepository(TextPageEntities db)
        {
            context = db;
        }
        
        public IList<TextPage> All
        {
            get { return context.TextPage.ToList(); }
        }

        public IQueryable<TextPage> AllIncluding(params Expression<Func<TextPage, object>>[] includeProperties)
        {
            IQueryable<TextPage> query = context.TextPage;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public TextPage Find(int id)
        {
            return context.TextPage.Find(id);
        }

        public void InsertOrUpdate(TextPage page)
        {
            if (page.Id == default(int))
            {
                // New entity
                context.TextPage.Add(page);
            }
            else
            {
                // Existing entity
                context.Entry(page).State = System.Data.Entity.EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var page = context.TextPage.Find(id);
            context.TextPage.Remove(page);
        }
        public void Save()
        {
            context.SaveChanges();
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}