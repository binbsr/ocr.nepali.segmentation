namespace OCR.Nepali.Segmentation.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<OCR.Nepali.Segmentation.Data.TextPageEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(OCR.Nepali.Segmentation.Data.TextPageEntities context)
        {
            //  This method will be called after migrating to the latest version.

            //You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //to avoid creating duplicate seed data. E.g.


            TextPage tp = new TextPage()
            {
                TextPageData = new TextPageData() { ImageData = new byte[] { 1, 2, 3 } },
                LinesSegmentedUsingPPP = 20,
                LinesSegmentedUsingModifiedPPP = 15,
                LinesCount = 22,
                Name = "Test1",
                WordsCount = 208
            };
            context.TextPage.AddOrUpdate(
              p => p.Id, tp);
            context.SaveChanges();
        }
    }
}