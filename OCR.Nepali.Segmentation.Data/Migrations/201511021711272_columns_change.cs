namespace OCR.Nepali.Segmentation.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class columns_change : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TextPages", "LinesSegmentedUsingPPP", c => c.Short(nullable: false));
            AddColumn("dbo.TextPages", "LinesSegmentedUsingModifiedPPP", c => c.Short(nullable: false));
            DropColumn("dbo.TextPages", "LinesCorrectlySegmented");
            DropColumn("dbo.TextPages", "Algorithm");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TextPages", "Algorithm", c => c.String());
            AddColumn("dbo.TextPages", "LinesCorrectlySegmented", c => c.Short(nullable: false));
            DropColumn("dbo.TextPages", "LinesSegmentedUsingModifiedPPP");
            DropColumn("dbo.TextPages", "LinesSegmentedUsingPPP");
        }
    }
}
