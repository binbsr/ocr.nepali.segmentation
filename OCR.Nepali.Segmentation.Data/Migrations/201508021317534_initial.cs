namespace OCR.Nepali.Segmentation.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TextPages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.String(),
                        LinesCount = c.Short(nullable: false),
                        WordsCount = c.Int(nullable: false),
                        CharsCount = c.Int(nullable: false),
                        ModifiersCount = c.Int(nullable: false),
                        LinesCorrectlySegmented = c.Short(nullable: false),
                        Algorithm = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TextPageDatas",
                c => new
                    {
                        TextPageId = c.Int(nullable: false),
                        ImageData = c.Binary(),
                    })
                .PrimaryKey(t => t.TextPageId)
                .ForeignKey("dbo.TextPages", t => t.TextPageId)
                .Index(t => t.TextPageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TextPageDatas", "TextPageId", "dbo.TextPages");
            DropIndex("dbo.TextPageDatas", new[] { "TextPageId" });
            DropTable("dbo.TextPageDatas");
            DropTable("dbo.TextPages");
        }
    }
}
