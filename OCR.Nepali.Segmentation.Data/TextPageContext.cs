﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCR.Nepali.Segmentation.Data
{
    public partial class TextPageEntities : DbContext
    {
        public TextPageEntities()
            : base()
        { }
        public DbSet<TextPage> TextPage { get; set; }
        public DbSet<TextPageData> TextPageData { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Textpage configuration
            //modelBuilder.Entity<TextPage>()                
            //    .Property(x => x.Id)
            //    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<TextPage>()
                .HasOptional(x => x.TextPageData)
                .WithRequired(t => t.TextPage);

            //TextpageData configuration
            modelBuilder.Entity<TextPageData>()
                //.HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasColumnName("TextPageId");          

        }
    }

}
