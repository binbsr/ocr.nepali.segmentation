﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCR.Nepali.Segmentation.Data
{
    public partial class TextPage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public short LinesCount { get; set; }
        public int WordsCount { get; set; }
        public int CharsCount { get; set; }
        public int ModifiersCount { get; set; }
        public short LinesSegmentedUsingPPP { get; set; }
        public short LinesSegmentedUsingModifiedPPP { get; set; }               
        
        public virtual TextPageData TextPageData { get; set; }
    }
}
