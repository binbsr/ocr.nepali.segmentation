﻿using AForge.Imaging.Filters;
using System.Drawing;

namespace NoiseReduction
{
    public class NoiseRemoval
    {
        public static Bitmap MedianSmoothing(Bitmap image)
        {
            // Create filter
            Median filter = new Median();            
            //Apply the filter
            filter.ApplyInPlace(image);
            return image;
        }

        public static Bitmap BilateralSmoothing(Bitmap image)
        {
            // create filter
            BilateralSmoothing filter = new BilateralSmoothing();
            filter.KernelSize = 7;
            filter.SpatialFactor = 10;
            filter.ColorFactor = 60;
            filter.ColorPower = 0.5;
            // apply the filter
            filter.ApplyInPlace(image);
            return image;
        }

        public static Bitmap ConservativeSmoothing(Bitmap image)
        {
            // create filter
            ConservativeSmoothing filter = new ConservativeSmoothing();
            // apply the filter
            filter.ApplyInPlace(image);
            return image;
        }

        public static Bitmap MeanSmoothing(Bitmap image)
        {
            // create filter
            Mean filter = new Mean();
            // apply the filter
            filter.ApplyInPlace(image);
            return image;
        }

        public static Bitmap AdaptiveSmoothing(Bitmap image)
        {
            // create filter
            AdaptiveSmoothing filter = new AdaptiveSmoothing();

            var cloned = AForge.Imaging.Image.Clone(image, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            // apply the filter
            filter.ApplyInPlace(cloned);
            return image;
        }
    }
}
