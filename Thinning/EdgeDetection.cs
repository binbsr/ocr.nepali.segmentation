﻿using AForge.Imaging.Filters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThinningAndEdgeDetection
{
    public class EdgeDetection
    {
        public static Bitmap HomogenityEdgeDetection(Bitmap sImage)
        {
            var image = AForge.Imaging.Image.Clone(sImage, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            // create filter
            HomogenityEdgeDetector filter = new HomogenityEdgeDetector();
            // apply the filter
            filter.ApplyInPlace(image);
            return image;
        }

        public static Bitmap SobelEdgeDetection(Bitmap sImage)
        {
            var image = AForge.Imaging.Image.Clone(sImage, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            // create filter
            SobelEdgeDetector filter = new SobelEdgeDetector();
            // apply the filter
            filter.ApplyInPlace(image);
            return image;
        }

        public static Bitmap CannyEdgeDetection(Bitmap sImage)
        {
            var image = AForge.Imaging.Image.Clone(sImage, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            // create filter
            CannyEdgeDetector filter = new CannyEdgeDetector();
            // apply the filter
            filter.ApplyInPlace(image);
            return image;
        }
    }
}
