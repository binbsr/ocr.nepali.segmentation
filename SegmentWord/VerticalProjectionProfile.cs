﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using SegmentLine;
using Accord.Math;
using Accord.Imaging.Converters;
using OCR.Nepali.Segmentation.Core;

namespace SegmentWord
{
    public class VerticalProjectionProfile
    {
        public static Dictionary<int, Byte[,]> LineBitmapArrays { get { return HorizontalProjectionProfile.lineBMs; } }
        public static Bitmap PageImage { get { return HorizontalProjectionProfile.wholeImage; } }
        public static List<Bitmap> WordBitmaps = new List<Bitmap>();

        internal class VPPIndexer
        {
            public IEnumerable<int> this[Byte[,] lineByteArray]
            {
                get
                {
                    for (int j = 0; j < lineByteArray.GetLength(1); j++)
                    {
                        var c = lineByteArray.GetColumn<byte>(j);
                        var blackPixelCount = c.Where(x => x == 0).Count();
                        yield return blackPixelCount;
                    }
                }
            }
        }

        /// <summary>
        /// Attaches word histogram with initial page image
        /// </summary>
        /// <returns></returns>
        public static Bitmap AttachWordHistos()
        {
            var histoGrammedBitmap = new Bitmap(PageImage);
            Graphics gr = Graphics.FromImage(histoGrammedBitmap);

            foreach (var line in LineBitmapArrays)
            {
                var bins = new VPPIndexer()[line.Value].ToList();
                int wordHeight = line.Value.GetLength(0);
                int wordHeightSum = wordHeight + line.Key;
                for (int x = 0; x < bins.Count; x++)
                {
                    float value = 125 * (float)bins[x] / wordHeight;
                    gr.DrawLine(Pens.Blue, new Point(x, wordHeightSum), new PointF(x, wordHeightSum - value));
                }
            }

            gr.Dispose();
            return histoGrammedBitmap;
        }

        /// <summary>
        /// Segments all the lines with in a page image
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, Dictionary<Point, byte[,]>> Segmentwords(out Bitmap rectangledWordsImage)
        {
            Dictionary<int, Dictionary<Point, byte[,]>> wordBitmaps = new Dictionary<int, Dictionary<Point, byte[,]>>();
            Dictionary<int, List<Bitmap>> lineWordsBitmaps = new Dictionary<int, List<Bitmap>>();
            rectangledWordsImage = new Bitmap(PageImage);
            Graphics g = Graphics.FromImage(rectangledWordsImage);

            foreach (var lineDict in LineBitmapArrays)
            {
                var line = lineDict.Value;
                List<int> wordPixels = new List<int>();

                //Getting vertical pixel distribution and filtering to relevant text word columns (Ignoring white spaces in between)
                List<int> bins = new VPPIndexer()[line].ToList();
                for (int i = 0; i < bins.Count; i++)
                {
                    if (bins[i] != 0)
                        wordPixels.Add(i);
                }

                //Clustering text lines on white spaces (i.e. discontinuity of row pixel data)
                List<List<int>> wordClusters = wordPixels.ExtractBins();

                //Wrap rectangles around individual word segments
                int height = line.GetLength(0);
                foreach (var bin in wordClusters)
                {
                    int width = bin[bin.Count - 1] - bin[0] + 1;
                    g.DrawRectangle(new Pen(Brushes.Red, 2),
                        new Rectangle(new Point(bin[0], lineDict.Key), new Size(width, height)));
                }

                //Word-byte-arrays(clusters elements) to image conversion      
                var wordBitmap = new Dictionary<Point, byte[,]>();
                wordClusters.ForEach(wcl =>
                {
                    Bitmap word;
                    var wordByteArray = line.SubMatrixCustom<byte>(null, wcl.ToArray());
                    wordBitmap.Add(new Point(wcl[0], lineDict.Key), wordByteArray);
                    new MatrixToImage().Convert(wordByteArray, out word);
                    WordBitmaps.Add(word);
                });

                wordBitmaps.Add(lineDict.Key, wordBitmap);
            }
            g.Dispose();

            return wordBitmaps;
        }
    }
}
