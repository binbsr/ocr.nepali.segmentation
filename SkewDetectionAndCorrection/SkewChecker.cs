﻿using AForge.Imaging;
using AForge.Imaging.Filters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkewDetectionAndCorrection
{
    public class SkewChecker
    {
        public static Bitmap Deskew(Bitmap docImage)
        {            
            var img = AForge.Imaging.Image.Clone(docImage, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            // create instance of skew checker
            DocumentSkewChecker skewChecker = new DocumentSkewChecker();
            // get documents skew angle
            double angle = skewChecker.GetSkewAngle(img);
            // create rotation filter
            RotateBilinear rotationFilter = new RotateBilinear(-angle);
            rotationFilter.FillColor = Color.White;
            // rotate image applying the filter
            img = rotationFilter.Apply(img);

            return img;
        }
    }
}
