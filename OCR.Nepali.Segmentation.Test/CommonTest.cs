﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using OCR.Nepali.Segmentation.Core;

namespace OCR.Nepali.Segmentation.Test
{
    [TestClass]
    public class CommonTest
    {
        [TestMethod]
        public void ExtractRowBinsTest()
        {
            // Arrange
            List<int> list = new List<int>() { 1, 2, 3, 4, 7, 8, 9, 10, 23, 24, 25, 26, 34, 35, 67, 68 };

            List<List<int>> expected = new List<List<int>>() 
            { 
                new List<int>(){1, 2, 3, 4}, 
                new List<int>(){7, 8, 9, 10},
                new List<int>(){23, 24, 25, 26},
                new List<int>(){34, 35},
                new List<int>(){67, 68}
            };

            // Act
            var actual = list.ExtractBins();

            //Accert
            Assert.IsTrue(expected.IsEqual(actual));
        }
        [TestMethod]
        public void SubMatrixxTest()
        {
            byte[,] matrix = {
                                {1, 2, 3, 4, 5},
                                {11, 12, 13, 14, 15},
                                {21, 22, 23, 24, 25},
                                {31, 32, 33, 34, 35},
                                {41, 42, 43, 44, 45},
                                {51, 52, 53, 54, 55},
                            };

            byte[,] expected = {                                                        
                                            {12, 13},
                                            {22, 23},
                                            {32, 33}                                            
                                        };
            int[] rowIndices = new[] { 1, 2, 3 };
            int[] colIndices = new[] { 1, 2};

            byte[,] actual = matrix.SubMatrixCustom<byte>(rowIndices, colIndices);

            Assert.IsTrue(actual.ContentEquals<byte>(expected));
        }
    }
}
