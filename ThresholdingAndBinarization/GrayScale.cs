﻿using System.Drawing;
using AForge.Imaging.Filters;
using ImageForge = AForge.Imaging;

namespace ThresholdingAndBinarization
{
    public class GrayScale
    {
        public static Bitmap GrayBT709(Bitmap image)
        {
            if (!ImageForge.Image.IsGrayscale(image))
            {
                return Grayscale.CommonAlgorithms.BT709.Apply(image);
            }
            return image;
        }

        public static Bitmap GrayRMY( Bitmap image)
        {
            if (!ImageForge.Image.IsGrayscale(image))
            {
                return Grayscale.CommonAlgorithms.RMY.Apply(image);
            }
            return image;
        }

        public static Bitmap GrayY(Bitmap image)
        {
            if (!ImageForge.Image.IsGrayscale(image))
            {
                return Grayscale.CommonAlgorithms.Y.Apply(image);
            }
            return image;
        }
    }
}
