﻿using AForge.Imaging.Filters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThresholdingAndBinarization
{
    public class BinarizationAdaptive
    {
        public static Bitmap Otsu(Bitmap image, ref byte threshold)
        {
            var img = AForge.Imaging.Image.Clone(image, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            OtsuThreshold filter = new OtsuThreshold();
            filter.ApplyInPlace(image);
            threshold = (Byte)filter.ThresholdValue;
            return image;
        }

        public static Bitmap BradelyLocalThreshold(Bitmap image)
        {
            var img = AForge.Imaging.Image.Clone(image, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            // create the filter
            BradleyLocalThresholding filter = new BradleyLocalThresholding();
            // apply the filter
            filter.ApplyInPlace(img);
            return img;
        }
    }
}
