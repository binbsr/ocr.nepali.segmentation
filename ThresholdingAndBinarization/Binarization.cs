﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge.Imaging.Filters;

namespace ThresholdingAndBinarization
{
    public class Binarization
    {
        public static Bitmap Threshold(Bitmap image)
        {            
            Threshold bin = new Threshold(110);
            bin.ApplyInPlace(image);
            return image;
        }
    }
}
