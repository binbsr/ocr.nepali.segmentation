﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OCR.Nepali.Segmentation.UI
{
    public partial class PageImage : Form
    {
        public PageImage(Image img)
        {
            InitializeComponent();
            pictureBox1.BackgroundImage = img;
        }
    }
}
