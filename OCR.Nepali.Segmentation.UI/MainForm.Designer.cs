﻿namespace OCR.Nepali.Segmentation.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmsNoise = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.noiseRemovalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bilateralToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.meanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conservativeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adaptiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testFRMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testPPPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testHTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbOriginal = new System.Windows.Forms.TabPage();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tbSingle = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tbBulk = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.lblFileName = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.tbNoise = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.cmsDeskew = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deskewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbSkew = new System.Windows.Forms.TabPage();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.cmsBinarize = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.binarizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otsuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bradelyLocalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbBinarize = new System.Windows.Forms.TabPage();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.cmsSegmentLines = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lineHistogramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detectEdgesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.homogenityEdgeDetectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobelEdgeDetectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cannyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.complexLayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbEdgeDetection = new System.Windows.Forms.TabPage();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.tbLineHisto = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tbLineHistogram = new System.Windows.Forms.TabPage();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.tbSegementedLines = new System.Windows.Forms.TabPage();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.cmsShowSegmentedLines = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showSegmentedLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbWordHistogram = new System.Windows.Forms.TabPage();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.tbSegmentedWords = new System.Windows.Forms.TabPage();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.cmsShowSegmentedWords = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showSegmentedWordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbComplexLayout = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tbFRM = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.tbPPP = new System.Windows.Forms.TabPage();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.tbHT = new System.Windows.Forms.TabPage();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.applicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gotoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grayScaleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bT709ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rMYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsNoise.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tbOriginal.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tbSingle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tbBulk.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tbNoise.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.cmsDeskew.SuspendLayout();
            this.tbSkew.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.cmsBinarize.SuspendLayout();
            this.tbBinarize.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.cmsSegmentLines.SuspendLayout();
            this.tbEdgeDetection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.tbLineHisto.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tbLineHistogram.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.tbSegementedLines.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.cmsShowSegmentedLines.SuspendLayout();
            this.tbWordHistogram.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.tbSegmentedWords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.cmsShowSegmentedWords.SuspendLayout();
            this.tbComplexLayout.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tbFRM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.tbPPP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.tbHT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.msMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmsNoise
            // 
            this.cmsNoise.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noiseRemovalToolStripMenuItem,
            this.testFRMToolStripMenuItem,
            this.testPPPToolStripMenuItem,
            this.testHTToolStripMenuItem});
            this.cmsNoise.Name = "contextMenuStrip1";
            this.cmsNoise.Size = new System.Drawing.Size(154, 92);
            // 
            // noiseRemovalToolStripMenuItem
            // 
            this.noiseRemovalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.medianToolStripMenuItem,
            this.bilateralToolStripMenuItem,
            this.meanToolStripMenuItem,
            this.conservativeToolStripMenuItem,
            this.adaptiveToolStripMenuItem});
            this.noiseRemovalToolStripMenuItem.Name = "noiseRemovalToolStripMenuItem";
            this.noiseRemovalToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.noiseRemovalToolStripMenuItem.Text = "Noise Removal";
            // 
            // medianToolStripMenuItem
            // 
            this.medianToolStripMenuItem.Name = "medianToolStripMenuItem";
            this.medianToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.medianToolStripMenuItem.Text = "Median";
            this.medianToolStripMenuItem.Click += new System.EventHandler(this.medianToolStripMenuItem_Click);
            // 
            // bilateralToolStripMenuItem
            // 
            this.bilateralToolStripMenuItem.Name = "bilateralToolStripMenuItem";
            this.bilateralToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.bilateralToolStripMenuItem.Text = "Bilateral";
            this.bilateralToolStripMenuItem.Click += new System.EventHandler(this.bilateralToolStripMenuItem_Click);
            // 
            // meanToolStripMenuItem
            // 
            this.meanToolStripMenuItem.Name = "meanToolStripMenuItem";
            this.meanToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.meanToolStripMenuItem.Text = "Mean";
            this.meanToolStripMenuItem.Click += new System.EventHandler(this.meanToolStripMenuItem_Click);
            // 
            // conservativeToolStripMenuItem
            // 
            this.conservativeToolStripMenuItem.Name = "conservativeToolStripMenuItem";
            this.conservativeToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.conservativeToolStripMenuItem.Text = "Conservative";
            this.conservativeToolStripMenuItem.Click += new System.EventHandler(this.conservativeToolStripMenuItem_Click);
            // 
            // adaptiveToolStripMenuItem
            // 
            this.adaptiveToolStripMenuItem.Name = "adaptiveToolStripMenuItem";
            this.adaptiveToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.adaptiveToolStripMenuItem.Text = "Adaptive";
            this.adaptiveToolStripMenuItem.Click += new System.EventHandler(this.adaptiveToolStripMenuItem_Click);
            // 
            // testFRMToolStripMenuItem
            // 
            this.testFRMToolStripMenuItem.Name = "testFRMToolStripMenuItem";
            this.testFRMToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.testFRMToolStripMenuItem.Text = "Test FRM";
            this.testFRMToolStripMenuItem.Click += new System.EventHandler(this.testFRMToolStripMenuItem_Click);
            // 
            // testPPPToolStripMenuItem
            // 
            this.testPPPToolStripMenuItem.Name = "testPPPToolStripMenuItem";
            this.testPPPToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.testPPPToolStripMenuItem.Text = "Test PPP";
            this.testPPPToolStripMenuItem.Click += new System.EventHandler(this.testPPPToolStripMenuItem_Click);
            // 
            // testHTToolStripMenuItem
            // 
            this.testHTToolStripMenuItem.Name = "testHTToolStripMenuItem";
            this.testHTToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.testHTToolStripMenuItem.Text = "Test HT";
            this.testHTToolStripMenuItem.Click += new System.EventHandler(this.testHTToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbOriginal);
            this.tabControl1.Controls.Add(this.tbNoise);
            this.tabControl1.Controls.Add(this.tbSkew);
            this.tabControl1.Controls.Add(this.tbBinarize);
            this.tabControl1.Controls.Add(this.tbEdgeDetection);
            this.tabControl1.Controls.Add(this.tbLineHisto);
            this.tabControl1.Controls.Add(this.tbComplexLayout);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1256, 518);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // tbOriginal
            // 
            this.tbOriginal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbOriginal.Controls.Add(this.tabControl4);
            this.tbOriginal.Location = new System.Drawing.Point(4, 22);
            this.tbOriginal.Name = "tbOriginal";
            this.tbOriginal.Padding = new System.Windows.Forms.Padding(3);
            this.tbOriginal.Size = new System.Drawing.Size(1248, 492);
            this.tbOriginal.TabIndex = 0;
            this.tbOriginal.Text = "Test Data";
            this.tbOriginal.UseVisualStyleBackColor = true;
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tbSingle);
            this.tabControl4.Controls.Add(this.tbBulk);
            this.tabControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl4.Location = new System.Drawing.Point(3, 3);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(1240, 484);
            this.tabControl4.TabIndex = 0;
            this.tabControl4.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl4_Selected);
            // 
            // tbSingle
            // 
            this.tbSingle.Controls.Add(this.pictureBox1);
            this.tbSingle.Location = new System.Drawing.Point(4, 22);
            this.tbSingle.Name = "tbSingle";
            this.tbSingle.Padding = new System.Windows.Forms.Padding(3);
            this.tbSingle.Size = new System.Drawing.Size(1232, 458);
            this.tbSingle.TabIndex = 0;
            this.tbSingle.Text = "Single Image Test";
            this.tbSingle.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.ContextMenuStrip = this.cmsNoise;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1226, 452);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // tbBulk
            // 
            this.tbBulk.Controls.Add(this.label6);
            this.tbBulk.Controls.Add(this.label12);
            this.tbBulk.Controls.Add(this.pictureBox17);
            this.tbBulk.Controls.Add(this.label11);
            this.tbBulk.Controls.Add(this.button4);
            this.tbBulk.Controls.Add(this.comboBox3);
            this.tbBulk.Controls.Add(this.button3);
            this.tbBulk.Controls.Add(this.numericUpDown5);
            this.tbBulk.Controls.Add(this.numericUpDown2);
            this.tbBulk.Controls.Add(this.numericUpDown4);
            this.tbBulk.Controls.Add(this.numericUpDown3);
            this.tbBulk.Controls.Add(this.lblFileName);
            this.tbBulk.Controls.Add(this.label10);
            this.tbBulk.Controls.Add(this.numericUpDown1);
            this.tbBulk.Controls.Add(this.label9);
            this.tbBulk.Controls.Add(this.label7);
            this.tbBulk.Controls.Add(this.label8);
            this.tbBulk.Controls.Add(this.label5);
            this.tbBulk.Controls.Add(this.label4);
            this.tbBulk.Controls.Add(this.label3);
            this.tbBulk.Controls.Add(this.textBox2);
            this.tbBulk.Controls.Add(this.textBox1);
            this.tbBulk.Controls.Add(this.flowLayoutPanel1);
            this.tbBulk.Location = new System.Drawing.Point(4, 22);
            this.tbBulk.Name = "tbBulk";
            this.tbBulk.Padding = new System.Windows.Forms.Padding(3);
            this.tbBulk.Size = new System.Drawing.Size(1232, 458);
            this.tbBulk.TabIndex = 1;
            this.tbBulk.Text = "All Image Data";
            this.tbBulk.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(51, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(204, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "No. of Correctly Segmented Lines (MPPP)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(51, 430);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 10;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox17.Location = new System.Drawing.Point(233, 340);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(144, 79);
            this.pictureBox17.TabIndex = 9;
            this.pictureBox17.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(51, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Load files from:";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(385, 36);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(31, 23);
            this.button4.TabIndex = 7;
            this.button4.Text = "Go";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Directory",
            "Database (Saved ones)"});
            this.comboBox3.Location = new System.Drawing.Point(135, 38);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(242, 21);
            this.comboBox3.TabIndex = 6;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(233, 425);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(144, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Save / Update";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.Location = new System.Drawing.Point(284, 245);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(93, 20);
            this.numericUpDown5.TabIndex = 3;
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(284, 208);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(93, 20);
            this.numericUpDown2.TabIndex = 3;
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Location = new System.Drawing.Point(233, 314);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(144, 20);
            this.numericUpDown4.TabIndex = 3;
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(233, 279);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(144, 20);
            this.numericUpDown3.TabIndex = 3;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(324, 352);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(0, 13);
            this.lblFileName.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(51, 351);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Image File";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(233, 172);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(144, 20);
            this.numericUpDown1.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(51, 316);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "No. of Characters";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(51, 210);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(195, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "No. of Correctly Segmented Lines (PPP)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(51, 281);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "No. of Words";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(51, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "No. of Lines";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Page Name";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(233, 133);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(144, 20);
            this.textBox2.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(233, 94);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(144, 20);
            this.textBox1.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(424, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(805, 452);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // tbNoise
            // 
            this.tbNoise.Controls.Add(this.pictureBox2);
            this.tbNoise.Location = new System.Drawing.Point(4, 22);
            this.tbNoise.Name = "tbNoise";
            this.tbNoise.Padding = new System.Windows.Forms.Padding(3);
            this.tbNoise.Size = new System.Drawing.Size(1248, 492);
            this.tbNoise.TabIndex = 1;
            this.tbNoise.Text = "Noise Removal";
            this.tbNoise.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.ContextMenuStrip = this.cmsDeskew;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1242, 486);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // cmsDeskew
            // 
            this.cmsDeskew.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deskewToolStripMenuItem});
            this.cmsDeskew.Name = "cmsDeskew";
            this.cmsDeskew.Size = new System.Drawing.Size(115, 26);
            // 
            // deskewToolStripMenuItem
            // 
            this.deskewToolStripMenuItem.Name = "deskewToolStripMenuItem";
            this.deskewToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.deskewToolStripMenuItem.Text = "Deskew";
            this.deskewToolStripMenuItem.Click += new System.EventHandler(this.deskewToolStripMenuItem_Click);
            // 
            // tbSkew
            // 
            this.tbSkew.Controls.Add(this.pictureBox4);
            this.tbSkew.Location = new System.Drawing.Point(4, 22);
            this.tbSkew.Name = "tbSkew";
            this.tbSkew.Size = new System.Drawing.Size(1248, 492);
            this.tbSkew.TabIndex = 3;
            this.tbSkew.Text = "Skew Detection";
            this.tbSkew.UseVisualStyleBackColor = true;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.ContextMenuStrip = this.cmsBinarize;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox4.Location = new System.Drawing.Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1248, 492);
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // cmsBinarize
            // 
            this.cmsBinarize.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.binarizeToolStripMenuItem});
            this.cmsBinarize.Name = "cmsBinarize";
            this.cmsBinarize.Size = new System.Drawing.Size(116, 26);
            // 
            // binarizeToolStripMenuItem
            // 
            this.binarizeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thresholdToolStripMenuItem,
            this.otsuToolStripMenuItem,
            this.bradelyLocalToolStripMenuItem});
            this.binarizeToolStripMenuItem.Name = "binarizeToolStripMenuItem";
            this.binarizeToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.binarizeToolStripMenuItem.Text = "Binarize";
            // 
            // thresholdToolStripMenuItem
            // 
            this.thresholdToolStripMenuItem.Name = "thresholdToolStripMenuItem";
            this.thresholdToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.thresholdToolStripMenuItem.Text = "Threshold";
            this.thresholdToolStripMenuItem.Click += new System.EventHandler(this.thresholdToolStripMenuItem_Click);
            // 
            // otsuToolStripMenuItem
            // 
            this.otsuToolStripMenuItem.Name = "otsuToolStripMenuItem";
            this.otsuToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.otsuToolStripMenuItem.Text = "Otsu";
            this.otsuToolStripMenuItem.Click += new System.EventHandler(this.otsuToolStripMenuItem_Click);
            // 
            // bradelyLocalToolStripMenuItem
            // 
            this.bradelyLocalToolStripMenuItem.Name = "bradelyLocalToolStripMenuItem";
            this.bradelyLocalToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.bradelyLocalToolStripMenuItem.Text = "Bradely Local Threshold";
            this.bradelyLocalToolStripMenuItem.Click += new System.EventHandler(this.bradelyLocalToolStripMenuItem_Click);
            // 
            // tbBinarize
            // 
            this.tbBinarize.Controls.Add(this.pictureBox3);
            this.tbBinarize.Location = new System.Drawing.Point(4, 22);
            this.tbBinarize.Name = "tbBinarize";
            this.tbBinarize.Size = new System.Drawing.Size(1248, 492);
            this.tbBinarize.TabIndex = 2;
            this.tbBinarize.Text = "Binarization";
            this.tbBinarize.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.ContextMenuStrip = this.cmsSegmentLines;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1248, 492);
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // cmsSegmentLines
            // 
            this.cmsSegmentLines.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lineHistogramToolStripMenuItem,
            this.detectEdgesToolStripMenuItem,
            this.complexLayoutToolStripMenuItem});
            this.cmsSegmentLines.Name = "cmsSegmentLines";
            this.cmsSegmentLines.Size = new System.Drawing.Size(240, 70);
            // 
            // lineHistogramToolStripMenuItem
            // 
            this.lineHistogramToolStripMenuItem.Name = "lineHistogramToolStripMenuItem";
            this.lineHistogramToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.lineHistogramToolStripMenuItem.Text = "Standard Histogram Separation";
            this.lineHistogramToolStripMenuItem.Click += new System.EventHandler(this.lineHistogramToolStripMenuItem_Click);
            // 
            // detectEdgesToolStripMenuItem
            // 
            this.detectEdgesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homogenityEdgeDetectionToolStripMenuItem,
            this.sobelEdgeDetectionToolStripMenuItem,
            this.cannyToolStripMenuItem});
            this.detectEdgesToolStripMenuItem.Name = "detectEdgesToolStripMenuItem";
            this.detectEdgesToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.detectEdgesToolStripMenuItem.Text = "Detect Edges";
            // 
            // homogenityEdgeDetectionToolStripMenuItem
            // 
            this.homogenityEdgeDetectionToolStripMenuItem.Name = "homogenityEdgeDetectionToolStripMenuItem";
            this.homogenityEdgeDetectionToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.homogenityEdgeDetectionToolStripMenuItem.Text = "Homogenity";
            this.homogenityEdgeDetectionToolStripMenuItem.Click += new System.EventHandler(this.homogenityEdgeDetectionToolStripMenuItem_Click);
            // 
            // sobelEdgeDetectionToolStripMenuItem
            // 
            this.sobelEdgeDetectionToolStripMenuItem.Name = "sobelEdgeDetectionToolStripMenuItem";
            this.sobelEdgeDetectionToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.sobelEdgeDetectionToolStripMenuItem.Text = "Sobel";
            this.sobelEdgeDetectionToolStripMenuItem.Click += new System.EventHandler(this.sobelEdgeDetectionToolStripMenuItem_Click);
            // 
            // cannyToolStripMenuItem
            // 
            this.cannyToolStripMenuItem.Name = "cannyToolStripMenuItem";
            this.cannyToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.cannyToolStripMenuItem.Text = "Canny";
            this.cannyToolStripMenuItem.Click += new System.EventHandler(this.cannyToolStripMenuItem_Click);
            // 
            // complexLayoutToolStripMenuItem
            // 
            this.complexLayoutToolStripMenuItem.Name = "complexLayoutToolStripMenuItem";
            this.complexLayoutToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.complexLayoutToolStripMenuItem.Text = "Complex Layout Nepali Text";
            this.complexLayoutToolStripMenuItem.Click += new System.EventHandler(this.complexLayoutToolStripMenuItem_Click);
            // 
            // tbEdgeDetection
            // 
            this.tbEdgeDetection.Controls.Add(this.pictureBox5);
            this.tbEdgeDetection.Location = new System.Drawing.Point(4, 22);
            this.tbEdgeDetection.Name = "tbEdgeDetection";
            this.tbEdgeDetection.Size = new System.Drawing.Size(1248, 492);
            this.tbEdgeDetection.TabIndex = 5;
            this.tbEdgeDetection.Text = "Edge Detection";
            this.tbEdgeDetection.UseVisualStyleBackColor = true;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(1248, 492);
            this.pictureBox5.TabIndex = 3;
            this.pictureBox5.TabStop = false;
            // 
            // tbLineHisto
            // 
            this.tbLineHisto.Controls.Add(this.tabControl2);
            this.tbLineHisto.Location = new System.Drawing.Point(4, 22);
            this.tbLineHisto.Name = "tbLineHisto";
            this.tbLineHisto.Size = new System.Drawing.Size(1248, 492);
            this.tbLineHisto.TabIndex = 4;
            this.tbLineHisto.Text = "Standard Projection Profile Separation";
            this.tbLineHisto.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tbLineHistogram);
            this.tabControl2.Controls.Add(this.tbSegementedLines);
            this.tabControl2.Controls.Add(this.tbWordHistogram);
            this.tabControl2.Controls.Add(this.tbSegmentedWords);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1248, 492);
            this.tabControl2.TabIndex = 0;
            // 
            // tbLineHistogram
            // 
            this.tbLineHistogram.Controls.Add(this.pictureBox6);
            this.tbLineHistogram.Location = new System.Drawing.Point(4, 22);
            this.tbLineHistogram.Name = "tbLineHistogram";
            this.tbLineHistogram.Padding = new System.Windows.Forms.Padding(3);
            this.tbLineHistogram.Size = new System.Drawing.Size(1240, 466);
            this.tbLineHistogram.TabIndex = 0;
            this.tbLineHistogram.Text = "Line Histogram";
            this.tbLineHistogram.UseVisualStyleBackColor = true;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox6.Location = new System.Drawing.Point(3, 3);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(1234, 460);
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // tbSegementedLines
            // 
            this.tbSegementedLines.Controls.Add(this.pictureBox7);
            this.tbSegementedLines.Location = new System.Drawing.Point(4, 22);
            this.tbSegementedLines.Name = "tbSegementedLines";
            this.tbSegementedLines.Padding = new System.Windows.Forms.Padding(3);
            this.tbSegementedLines.Size = new System.Drawing.Size(1240, 466);
            this.tbSegementedLines.TabIndex = 1;
            this.tbSegementedLines.Text = "Segmented Lines";
            this.tbSegementedLines.UseVisualStyleBackColor = true;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox7.ContextMenuStrip = this.cmsShowSegmentedLines;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox7.Location = new System.Drawing.Point(3, 3);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(1234, 460);
            this.pictureBox7.TabIndex = 2;
            this.pictureBox7.TabStop = false;
            // 
            // cmsShowSegmentedLines
            // 
            this.cmsShowSegmentedLines.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showSegmentedLinesToolStripMenuItem});
            this.cmsShowSegmentedLines.Name = "cmsShowSegmentedLines";
            this.cmsShowSegmentedLines.Size = new System.Drawing.Size(197, 26);
            // 
            // showSegmentedLinesToolStripMenuItem
            // 
            this.showSegmentedLinesToolStripMenuItem.Name = "showSegmentedLinesToolStripMenuItem";
            this.showSegmentedLinesToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.showSegmentedLinesToolStripMenuItem.Text = "Show Segmented Lines";
            this.showSegmentedLinesToolStripMenuItem.Click += new System.EventHandler(this.showSegmentedLinesToolStripMenuItem_Click);
            // 
            // tbWordHistogram
            // 
            this.tbWordHistogram.Controls.Add(this.pictureBox8);
            this.tbWordHistogram.Location = new System.Drawing.Point(4, 22);
            this.tbWordHistogram.Name = "tbWordHistogram";
            this.tbWordHistogram.Size = new System.Drawing.Size(1240, 466);
            this.tbWordHistogram.TabIndex = 2;
            this.tbWordHistogram.Text = "Word Histogram";
            this.tbWordHistogram.UseVisualStyleBackColor = true;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox8.Location = new System.Drawing.Point(0, 0);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(1240, 466);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 1;
            this.pictureBox8.TabStop = false;
            // 
            // tbSegmentedWords
            // 
            this.tbSegmentedWords.Controls.Add(this.pictureBox9);
            this.tbSegmentedWords.Location = new System.Drawing.Point(4, 22);
            this.tbSegmentedWords.Name = "tbSegmentedWords";
            this.tbSegmentedWords.Size = new System.Drawing.Size(1240, 466);
            this.tbSegmentedWords.TabIndex = 3;
            this.tbSegmentedWords.Text = "Segmented Words";
            this.tbSegmentedWords.UseVisualStyleBackColor = true;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox9.ContextMenuStrip = this.cmsShowSegmentedWords;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox9.Location = new System.Drawing.Point(0, 0);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(1240, 466);
            this.pictureBox9.TabIndex = 1;
            this.pictureBox9.TabStop = false;
            // 
            // cmsShowSegmentedWords
            // 
            this.cmsShowSegmentedWords.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showSegmentedWordsToolStripMenuItem});
            this.cmsShowSegmentedWords.Name = "cmsShowSegmentedWords";
            this.cmsShowSegmentedWords.Size = new System.Drawing.Size(204, 26);
            // 
            // showSegmentedWordsToolStripMenuItem
            // 
            this.showSegmentedWordsToolStripMenuItem.Name = "showSegmentedWordsToolStripMenuItem";
            this.showSegmentedWordsToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.showSegmentedWordsToolStripMenuItem.Text = "Show Segmented Words";
            this.showSegmentedWordsToolStripMenuItem.Click += new System.EventHandler(this.showSegmentedWordsToolStripMenuItem_Click);
            // 
            // tbComplexLayout
            // 
            this.tbComplexLayout.Controls.Add(this.tabControl3);
            this.tbComplexLayout.Location = new System.Drawing.Point(4, 22);
            this.tbComplexLayout.Name = "tbComplexLayout";
            this.tbComplexLayout.Size = new System.Drawing.Size(1248, 492);
            this.tbComplexLayout.TabIndex = 6;
            this.tbComplexLayout.Text = "Complex Layout Nepali Text";
            this.tbComplexLayout.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tbFRM);
            this.tabControl3.Controls.Add(this.tbPPP);
            this.tabControl3.Controls.Add(this.tbHT);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(0, 0);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(1248, 492);
            this.tabControl3.TabIndex = 0;
            // 
            // tbFRM
            // 
            this.tbFRM.Controls.Add(this.button1);
            this.tbFRM.Controls.Add(this.label2);
            this.tbFRM.Controls.Add(this.label1);
            this.tbFRM.Controls.Add(this.comboBox2);
            this.tbFRM.Controls.Add(this.comboBox1);
            this.tbFRM.Controls.Add(this.pictureBox14);
            this.tbFRM.Controls.Add(this.pictureBox10);
            this.tbFRM.Location = new System.Drawing.Point(4, 22);
            this.tbFRM.Name = "tbFRM";
            this.tbFRM.Padding = new System.Windows.Forms.Padding(3);
            this.tbFRM.Size = new System.Drawing.Size(1240, 466);
            this.tbFRM.TabIndex = 0;
            this.tbFRM.Text = "Fuzzy Runlength Map (FRM)";
            this.tbFRM.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(576, 177);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Apply";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(573, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Binarization threshold";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(573, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Max Block Count";
            // 
            // comboBox2
            // 
            this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "50",
            "75",
            "100",
            "150",
            "180",
            "200"});
            this.comboBox2.Location = new System.Drawing.Point(576, 133);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 2;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "10",
            "20",
            "30",
            "40",
            "50"});
            this.comboBox1.Location = new System.Drawing.Point(576, 86);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox14.Location = new System.Drawing.Point(3, 3);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(555, 460);
            this.pictureBox14.TabIndex = 1;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox10.Location = new System.Drawing.Point(714, 3);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(523, 460);
            this.pictureBox10.TabIndex = 0;
            this.pictureBox10.TabStop = false;
            // 
            // tbPPP
            // 
            this.tbPPP.Controls.Add(this.pictureBox16);
            this.tbPPP.Controls.Add(this.pictureBox15);
            this.tbPPP.Controls.Add(this.pictureBox11);
            this.tbPPP.Location = new System.Drawing.Point(4, 22);
            this.tbPPP.Name = "tbPPP";
            this.tbPPP.Padding = new System.Windows.Forms.Padding(3);
            this.tbPPP.Size = new System.Drawing.Size(1240, 466);
            this.tbPPP.TabIndex = 1;
            this.tbPPP.Text = "Piecewise Projection Profile (PPP)";
            this.tbPPP.UseVisualStyleBackColor = true;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox16.Location = new System.Drawing.Point(423, 3);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(390, 460);
            this.pictureBox16.TabIndex = 2;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.DoubleClick += new System.EventHandler(this.pb_DoubleClick);
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox15.Location = new System.Drawing.Point(3, 3);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(422, 460);
            this.pictureBox15.TabIndex = 1;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.DoubleClick += new System.EventHandler(this.pb_DoubleClick);
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox11.Location = new System.Drawing.Point(811, 3);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(426, 460);
            this.pictureBox11.TabIndex = 0;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.DoubleClick += new System.EventHandler(this.pb_DoubleClick);
            // 
            // tbHT
            // 
            this.tbHT.Controls.Add(this.pictureBox13);
            this.tbHT.Controls.Add(this.pictureBox12);
            this.tbHT.Location = new System.Drawing.Point(4, 22);
            this.tbHT.Name = "tbHT";
            this.tbHT.Size = new System.Drawing.Size(1240, 466);
            this.tbHT.TabIndex = 2;
            this.tbHT.Text = "Hough Transform (HT)";
            this.tbHT.UseVisualStyleBackColor = true;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox13.Location = new System.Drawing.Point(648, 0);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(592, 466);
            this.pictureBox13.TabIndex = 1;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox12.Location = new System.Drawing.Point(0, 0);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(591, 466);
            this.pictureBox12.TabIndex = 0;
            this.pictureBox12.TabStop = false;
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applicationToolStripMenuItem,
            this.gotoToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(1256, 24);
            this.msMain.TabIndex = 2;
            this.msMain.Text = "menuStrip1";
            // 
            // applicationToolStripMenuItem
            // 
            this.applicationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.applicationToolStripMenuItem.Name = "applicationToolStripMenuItem";
            this.applicationToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.applicationToolStripMenuItem.Text = "Application";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // gotoToolStripMenuItem
            // 
            this.gotoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.grayScaleToolStripMenuItem1});
            this.gotoToolStripMenuItem.Name = "gotoToolStripMenuItem";
            this.gotoToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.gotoToolStripMenuItem.Text = "Settings";
            // 
            // grayScaleToolStripMenuItem1
            // 
            this.grayScaleToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bT709ToolStripMenuItem,
            this.rMYToolStripMenuItem,
            this.yToolStripMenuItem});
            this.grayScaleToolStripMenuItem1.Name = "grayScaleToolStripMenuItem1";
            this.grayScaleToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.grayScaleToolStripMenuItem1.Text = "Grayscale";
            // 
            // bT709ToolStripMenuItem
            // 
            this.bT709ToolStripMenuItem.CheckOnClick = true;
            this.bT709ToolStripMenuItem.Name = "bT709ToolStripMenuItem";
            this.bT709ToolStripMenuItem.Overflow = System.Windows.Forms.ToolStripItemOverflow.Always;
            this.bT709ToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.bT709ToolStripMenuItem.Text = "BT709";
            this.bT709ToolStripMenuItem.ToolTipText = "Grayscale image using BT709 algorithm.";
            this.bT709ToolStripMenuItem.Click += new System.EventHandler(this.bT709ToolStripMenuItem_Click);
            // 
            // rMYToolStripMenuItem
            // 
            this.rMYToolStripMenuItem.Checked = true;
            this.rMYToolStripMenuItem.CheckOnClick = true;
            this.rMYToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rMYToolStripMenuItem.Name = "rMYToolStripMenuItem";
            this.rMYToolStripMenuItem.Overflow = System.Windows.Forms.ToolStripItemOverflow.Always;
            this.rMYToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.rMYToolStripMenuItem.Text = "RMY";
            this.rMYToolStripMenuItem.ToolTipText = "Grayscale image using R-Y algorithm.";
            // 
            // yToolStripMenuItem
            // 
            this.yToolStripMenuItem.CheckOnClick = true;
            this.yToolStripMenuItem.Name = "yToolStripMenuItem";
            this.yToolStripMenuItem.Overflow = System.Windows.Forms.ToolStripItemOverflow.Always;
            this.yToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.yToolStripMenuItem.Text = "Y";
            this.yToolStripMenuItem.ToolTipText = "Grayscale image using Y algorithm.";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1256, 542);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.msMain);
            this.HelpButton = true;
            this.MainMenuStrip = this.msMain;
            this.MinimumSize = new System.Drawing.Size(1272, 581);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nepali Text Line, Word and Character Segmenter";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.cmsNoise.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tbOriginal.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tbSingle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tbBulk.ResumeLayout(false);
            this.tbBulk.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tbNoise.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.cmsDeskew.ResumeLayout(false);
            this.tbSkew.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.cmsBinarize.ResumeLayout(false);
            this.tbBinarize.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.cmsSegmentLines.ResumeLayout(false);
            this.tbEdgeDetection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.tbLineHisto.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tbLineHistogram.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.tbSegementedLines.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.cmsShowSegmentedLines.ResumeLayout(false);
            this.tbWordHistogram.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.tbSegmentedWords.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.cmsShowSegmentedWords.ResumeLayout(false);
            this.tbComplexLayout.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tbFRM.ResumeLayout(false);
            this.tbFRM.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.tbPPP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.tbHT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip cmsNoise;
        private System.Windows.Forms.ToolStripMenuItem noiseRemovalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bilateralToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem meanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conservativeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adaptiveToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbOriginal;
        private System.Windows.Forms.TabPage tbNoise;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem applicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gotoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grayScaleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage tbBinarize;
        private System.Windows.Forms.TabPage tbSkew;
        private System.Windows.Forms.ContextMenuStrip cmsBinarize;
        private System.Windows.Forms.ToolStripMenuItem binarizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thresholdToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ToolStripMenuItem otsuToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsDeskew;
        private System.Windows.Forms.ToolStripMenuItem deskewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bradelyLocalToolStripMenuItem;
        private System.Windows.Forms.TabPage tbLineHisto;
        private System.Windows.Forms.TabPage tbEdgeDetection;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.ContextMenuStrip cmsSegmentLines;
        private System.Windows.Forms.ToolStripMenuItem lineHistogramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detectEdgesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem homogenityEdgeDetectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobelEdgeDetectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cannyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bT709ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rMYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsShowSegmentedLines;
        private System.Windows.Forms.ToolStripMenuItem showSegmentedLinesToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsShowSegmentedWords;
        private System.Windows.Forms.ToolStripMenuItem showSegmentedWordsToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tbLineHistogram;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TabPage tbSegementedLines;
        private System.Windows.Forms.TabPage tbWordHistogram;
        private System.Windows.Forms.TabPage tbSegmentedWords;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.TabPage tbComplexLayout;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tbFRM;
        private System.Windows.Forms.TabPage tbPPP;
        private System.Windows.Forms.TabPage tbHT;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.ToolStripMenuItem complexLayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testFRMToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tbSingle;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tbBulk;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.ToolStripMenuItem testPPPToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.ToolStripMenuItem testHTToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown5;

    }
}