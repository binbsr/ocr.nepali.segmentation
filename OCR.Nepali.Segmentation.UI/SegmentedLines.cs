﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OCR.Nepali.Segmentation.UI
{
    public partial class SegmentedLines : Form
    {
        public SegmentedLines(List<Bitmap> bitmaps)
        {
            InitializeComponent();
            bitmaps.ForEach(line =>
            {
                PictureBox p = new PictureBox();
                p.BorderStyle = BorderStyle.Fixed3D;
                p.Size = new System.Drawing.Size(400, 50);
                p.BackgroundImageLayout = ImageLayout.Zoom;
                p.BackgroundImage = line;
                p.Parent = flowLayoutPanel1;
            });
        }       
    }
}
