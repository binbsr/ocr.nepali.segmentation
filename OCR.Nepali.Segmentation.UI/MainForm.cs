﻿using System;
using System.Windows.Forms;
using System.Drawing;
using NoiseReduction;
using ThresholdingAndBinarization;
using SkewDetectionAndCorrection;
using SegmentLine;
using ThinningAndEdgeDetection;
using SegmentWord;
using System.IO;
using OCR.Nepali.Segmentation.Data;
using Accord.Imaging.Converters;
using System.Data;
using OCR.Nepali.Segmentation.Core;
using System.Linq;
using System.Collections.Generic;
using OCR.Nepali.Segmentation.Data.ViewModels;

namespace OCR.Nepali.Segmentation.UI
{
    public partial class MainForm : Form
    {
        //Input image
        public Bitmap ImageOriginal { get { return (Bitmap)pictureBox1.BackgroundImage; } }
        public Bitmap ImageToNoiseRemove { get { return (Bitmap)pictureBox1.BackgroundImage; } }
        public Bitmap ImageToBinarize { get { return (Bitmap)pictureBox4.BackgroundImage; } }
        public Bitmap ImageToDeskew { get { return GrayScale.GrayBT709((Bitmap)pictureBox2.BackgroundImage); } }
        public Bitmap ImageForHistogram { get { return BinarizationAdaptive.BradelyLocalThreshold(GrayScale.GrayBT709(ImageOriginal)); } }

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            pictureBox1.BackgroundImage = ImageOriginal;
        }

        #region NoiseRemoval
        private void medianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbNoise");
            pictureBox2.BackgroundImage = NoiseRemoval.MedianSmoothing(ImageToNoiseRemove);
        }

        private void bilateralToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbNoise");
            pictureBox2.BackgroundImage = NoiseRemoval.BilateralSmoothing(ImageToNoiseRemove);
        }

        private void meanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbNoise");
            pictureBox2.BackgroundImage = NoiseRemoval.MeanSmoothing(ImageToNoiseRemove);
        }

        private void conservativeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbNoise");
            pictureBox2.BackgroundImage = NoiseRemoval.ConservativeSmoothing(ImageToNoiseRemove);
        }

        private void adaptiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbNoise");
            pictureBox2.BackgroundImage = NoiseRemoval.AdaptiveSmoothing(ImageToNoiseRemove);
        }
        #endregion

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void thresholdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbBinarize");
            pictureBox3.BackgroundImage = Binarization.Threshold(ImageToBinarize);
        }

        private void otsuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbBinarize");
            byte threshold = 0;
            pictureBox3.BackgroundImage = BinarizationAdaptive.Otsu(ImageToBinarize, ref threshold);
        }

        private void bradelyLocalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbBinarize");
            pictureBox3.BackgroundImage = BinarizationAdaptive.BradelyLocalThreshold(ImageToBinarize);
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            pictureBox1.BackgroundImage = ImageOriginal;
        }

        private void deskewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbSkew");
            pictureBox4.BackgroundImage = SkewChecker.Deskew(ImageToDeskew);
        }

        private void lineHistogramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Histogram hisVertical = HorizontalProjectionProfile.VerticalIntensities(ImageForHistogram).Gray;
            //Histogram hisHorizontal = HorizontalProjectionProfile.HorizontalIntensities(ImageForHistogram).Gray;
            //var verValues = hisVertical.Values;
            //var horValues = hisHorizontal.Values;  

            tabControl1.SelectTab("tbLineHisto");
            //Same page bitmap with row-intensity (Y-axis) histogram added
            pictureBox6.BackgroundImage = ImageForHistogram.AttachLineHistos();

            Bitmap imageLineRectangles;
            Bitmap imageWordRectangles;
            ImageForHistogram.SegmentLines(out imageLineRectangles);
            //ForEach(line =>
            //{
            //    PictureBox p = new PictureBox();
            //    p.BorderStyle = BorderStyle.Fixed3D;
            //    p.Size = new System.Drawing.Size(400, 50);
            //    p.BackgroundImageLayout = ImageLayout.Zoom;
            //    p.BackgroundImage = line;
            //    p.Parent = flowLayoutPanel1;
            //});

            pictureBox7.BackgroundImage = imageLineRectangles;
            VerticalProjectionProfile.Segmentwords(out imageWordRectangles);
            pictureBox8.BackgroundImage = VerticalProjectionProfile.AttachWordHistos();
            pictureBox9.BackgroundImage = imageWordRectangles;
        }

        private void homogenityEdgeDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbEdgeDetection");
            pictureBox5.BackgroundImage = EdgeDetection.HomogenityEdgeDetection(ImageForHistogram);
        }

        private void sobelEdgeDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbEdgeDetection");
            pictureBox5.BackgroundImage = EdgeDetection.SobelEdgeDetection(ImageForHistogram);
        }

        private void cannyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbEdgeDetection");
            pictureBox5.BackgroundImage = EdgeDetection.CannyEdgeDetection(ImageForHistogram);
        }

        private void bT709ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var items = grayScaleToolStripMenuItem1.DropDownItems;
            items.Remove((ToolStripMenuItem)sender);
            foreach (ToolStripMenuItem toolStrip in items)
            {
                if (toolStrip.CheckState == CheckState.Checked)
                    toolStrip.CheckState = CheckState.Unchecked;
            }
        }

        private void showSegmentedLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SegmentedLines lineForm = new SegmentedLines(HorizontalProjectionProfile.LineBitmaps);
            lineForm.Show();
        }

        private void showSegmentedWordsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SegmentedWords wordForm = new SegmentedWords(VerticalProjectionProfile.WordBitmaps);
            wordForm.Show();
        }

        private void complexLayoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap img;
            tabControl1.SelectTab("tbComplexLayout");
            pictureBox10.BackgroundImage = FuzzyRunlengthMap.Build(ImageOriginal, out img);
        }

        private void testFRMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap img;
            pictureBox14.BackgroundImage = ImageOriginal;
            tabControl1.SelectTab("tbComplexLayout");
            ((TabControl)(tabControl1.TabPages[6].Controls[0])).SelectTab("tbFRM");
            pictureBox10.BackgroundImage = FuzzyRunlengthMap.Build(ImageForHistogram, out img);
        }

        private void tabControl4_Selected(object sender, TabControlEventArgs e)
        {
            //if (e.TabPageIndex == 1)
            //{
            //    flowLayoutPanel1.Controls.Clear();
            //    var dir = new DirectoryInfo(Path.GetDirectoryName(Application.ExecutablePath));
            //    var datasetpath = new DirectoryInfo(Path.Combine(dir.Parent.Parent.FullName, "Dataset"));
            //    var files = datasetpath.GetFiles();

            //    foreach (var file in files)
            //    {
            //        PictureBox pb = new PictureBox();
            //        pb.Size = new System.Drawing.Size(200, 300);
            //        pb.BackgroundImageLayout = ImageLayout.Zoom;
            //        pb.BackgroundImage = Bitmap.FromFile(file.FullName);
            //        pb.Parent = flowLayoutPanel1;
            //    }
            //}
        }

        private void testPPPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap profileImage, forFuzzyOrgImage;
            tabControl1.SelectTab("tbComplexLayout");
            ((TabControl)(tabControl1.TabPages[6].Controls[0])).SelectTab("tbPPP");
            pictureBox15.BackgroundImage = ImageOriginal;
            var imageFrm = FuzzyRunlengthMap.Build(ImageOriginal, out forFuzzyOrgImage);
            pictureBox11.BackgroundImage = PiecewiseProjectionProfile.Build(imageFrm, forFuzzyOrgImage, out profileImage);
            pictureBox16.BackgroundImage = PiecewiseProjectionProfile.Build(ImageOriginal, forFuzzyOrgImage, out profileImage);
        }

        private void testHTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap hLinedImage;
            tabControl1.SelectTab("tbComplexLayout");
            ((TabControl)(tabControl1.TabPages[6].Controls[0])).SelectTab("tbHT");
            pictureBox12.BackgroundImage = HoughTransform.Build(ImageOriginal, out hLinedImage);
            pictureBox13.BackgroundImage = hLinedImage;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap img;
            var maxBlkCount = int.Parse(comboBox1.Text);
            var binThreshold = int.Parse(comboBox2.Text);
            pictureBox10.BackgroundImage = FuzzyRunlengthMap.Build(ImageOriginal, out img, maxBlkCount, binThreshold);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();
            if (comboBox3.SelectedIndex == 0)
            {
                OpenFileDialog files = new OpenFileDialog();
                files.Multiselect = true;
                files.Filter = "Image Files (*.jpg;*.bmp;*.gif;*.png)|*.jpg;*.bmp;*.gif;*.png|All Files (*.*)|*.*";
                files.ShowDialog();
                var fileNames = files.FileNames;

                foreach (var fileName in fileNames)
                {
                    FileInfo file = new FileInfo(fileName);
                    PictureBox pb = new PictureBox();
                    ToolTip t = new ToolTip();
                    var image = Bitmap.FromFile(fileName);
                    image.Tag = new TextPageViewModel { Name = file.Name };
                    t.ToolTipTitle = "Text Page Image";
                    t.IsBalloon = true;
                    t.SetToolTip(pb, "Name: " + file.Name + "\nSize: " + file.Length / 1024 + "KB");
                    pb.Size = new System.Drawing.Size(200, 300);
                    pb.BackgroundImageLayout = ImageLayout.Zoom;
                    pb.BackgroundImage = image;
                    pb.Parent = flowLayoutPanel1;
                    pb.Click += pb_Click;
                    pb.DoubleClick += pb_DoubleClick;
                }
            }
            else if (comboBox3.SelectedIndex == 1)
            {
                //Fetch from database
                IList<TextPage> allPages = null;
                using (TextPageRepository repo = new TextPageRepository(new TextPageEntities()))
                {
                    allPages = repo.AllIncluding(x => x.TextPageData).ToList();
                }

                foreach (var page in allPages)
                {
                    PictureBox pbDb = new PictureBox();
                    ToolTip tip = new ToolTip();
                    var imageArr = page.TextPageData.ImageData;
                    var image = ImageHandler.ArrayToImage(imageArr);
                    image.Tag = new TextPageViewModel { Name = page.Name, Type = page.Type, 
                        LinesCount = page.LinesCount, LinesSegmentedUsingPPP = page.LinesSegmentedUsingPPP, 
                        LinesSegmentedUsingModifiedPPP = page.LinesSegmentedUsingModifiedPPP, 
                        CharsCount = page.CharsCount, WordsCount = page.WordsCount };
                    pbDb.BackgroundImage = image;
                    tip.ToolTipTitle = page.Name;
                    tip.SetToolTip(pbDb, "Size : " + imageArr.Length / (1024) + " Kb" +
                        Environment.NewLine + "Lines : " + page.LinesCount +
                        Environment.NewLine + "Words : " + page.WordsCount +
                        Environment.NewLine + "Lines (PPP) : " + page.LinesSegmentedUsingPPP +
                        Environment.NewLine + "Lines (MPPP) : " + page.LinesSegmentedUsingModifiedPPP);
                    pbDb.Size = new System.Drawing.Size(100, 150);
                    pbDb.BackgroundImageLayout = ImageLayout.Zoom;
                    pbDb.Parent = flowLayoutPanel1;
                    pbDb.Click += pb_Click;
                    pbDb.DoubleClick += pb_DoubleClick;
                }
            }
        }

        void pb_DoubleClick(object sender, EventArgs e)
        {
            PageImage pImage = new PageImage(((PictureBox)sender).BackgroundImage);
            pImage.StartPosition = FormStartPosition.CenterParent;
            pImage.Show();
        }

        void pb_Click(object sender, EventArgs e)
        {
            var pictureBox = ((PictureBox)sender);
            var imageBackground = pictureBox.BackgroundImage;
            var tagObj = (TextPageViewModel)imageBackground.Tag;
            textBox1.Text = tagObj.Name;
            textBox2.Text = tagObj.Type ?? "Undefined";
            numericUpDown1.Value = (tagObj.LinesCount != default(short)) ? tagObj.LinesCount : 0;
            numericUpDown2.Value = (tagObj.LinesSegmentedUsingPPP != default(short)) ? tagObj.LinesSegmentedUsingPPP : 0;
            numericUpDown5.Value = (tagObj.LinesSegmentedUsingModifiedPPP != default(short)) ? tagObj.LinesSegmentedUsingModifiedPPP: 0;
            numericUpDown3.Value = (tagObj.WordsCount != default(int)) ? (decimal)tagObj.WordsCount : decimal.Zero;
            numericUpDown4.Value = (tagObj.CharsCount != default(int)) ? (decimal)tagObj.CharsCount : decimal.Zero;
            pictureBox1.BackgroundImage = imageBackground;
            pictureBox17.BackgroundImage = imageBackground;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Collecting data
            var name = textBox1.Text;
            var type = textBox2.Text;
            var linesCount = (byte)numericUpDown1.Value;
            var linesCorrectlySegmentedPPP = (byte)numericUpDown2.Value;
            var linesCorrectlySegmentedMPPP = (byte)numericUpDown5.Value;
            var wordsCount = (int)numericUpDown3.Value;
            var charsCount = (int)numericUpDown4.Value;
            var image = pictureBox17.BackgroundImage;
            try
            {
                using (TextPageRepository db = new TextPageRepository(new TextPageEntities()))
                {
                    var textpageData = new TextPageData() { ImageData = ImageHandler.ImageToArray(image) };
                    var pageInfo = new TextPage()
                    {
                        Name = name,
                        Type = type,
                        LinesCount = linesCount,
                        LinesSegmentedUsingPPP = linesCorrectlySegmentedPPP,
                        LinesSegmentedUsingModifiedPPP = linesCorrectlySegmentedMPPP,
                        WordsCount = wordsCount,
                        CharsCount = charsCount,
                        TextPageData = textpageData,
                    };
                    db.InsertOrUpdate(pageInfo);
                    db.Save();
                }
                label12.Text = name + " Saved!";
                label12.ForeColor = Color.Green;
            }
            catch (Exception err)
            {
                label12.Text = "Something went wrong: " + err.Message;
                label12.ForeColor = Color.Red;
            }
        }
    }
}