﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace OCR.Nepali.Segmentation.UI
{
    public partial class SegmentedWords : Form
    {
        public SegmentedWords(List<Bitmap> bitmaps)
        {
            InitializeComponent();
            bitmaps.ForEach(word =>
            {
                PictureBox p = new PictureBox();
                p.BorderStyle = BorderStyle.Fixed3D;                
                p.BackgroundImageLayout = ImageLayout.Zoom;
                p.BackgroundImage = word;
                p.Parent = flowLayoutPanel1;
            });
        }
    }
}
